<?php

/**
 * @file
 * Chado connection user interface controller class definition.
 */

/**
 * User interface controller for Chado connections.
 */
class TripalMCConnectionUIController extends EntityDefaultUIController {

  /**
   * Returns an overview table of available Chado connection.
   *
   * @return string
   *   A rendered (HTML) Chado connection overview table.
   */
  public function overviewTable($conditions = array()) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', $this->entityType);

    // Add all conditions to query.
    foreach ($conditions as $key => $value) {
      $query->propertyCondition($key, $value);
    }

    if ($this->overviewPagerLimit) {
      $query->pager($this->overviewPagerLimit);
    }
    $results = $query->execute();

    if (!empty($results)) {
      // Load data about each connection.
      $entities = entity_load(TRIPAL_MC_CONNECTION_ENTITY_TYPE, array_keys($results[TRIPAL_MC_CONNECTION_ENTITY_TYPE]));

      // Create the table.
      $rows = array();
      foreach ($entities as $mccid => $entity) {
        $user_list = array();
        if (isset($entity->{TRIPAL_MC_ASSOCIATED_USER_FIELD}[LANGUAGE_NONE])) {
          foreach ($entity->{TRIPAL_MC_ASSOCIATED_USER_FIELD}[LANGUAGE_NONE] as $associated_user) {
            $associated_user_object = user_load($associated_user['target_id']);
            $user_list[] = l($associated_user_object->name, 'user/' . $associated_user_object->uid);
          }
        }

        $title = l($entity->title, 'tripal_mc_connection/' . $entity->mccid);
        $operations =
          '<ul class="links inline"><li class="edit first">'
          . l(t('edit'), 'tripal_mc_connection/' . $entity->mccid . '/edit')
          . '</li><li class="delete last">'
          . l(t('delete'), 'tripal_mc_connection/' . $entity->mccid . '/delete')
          . '</li></ul>';
        $associated_users = (!empty($user_list) ? implode(', ', $user_list) : '');
        $rows[$mccid] = array(
          'title'            => $title,
          'username'         => $entity->username,
          'url'              => $entity->host . ':' . $entity->port,
          'database'         => $entity->database,
          'schema'           => $entity->schemaname,
          'associated_users' => $associated_users,
          'operations'       => $operations,
        );
      }

      return theme(
        'table',
        array(
          'header' =>
            array(
              'title'            => t('Name'),
              'username'         => t('Username'),
              'Server'           => t('Server'),
              'database'         => t('Database'),
              'schema'           => t('Schema'),
              'associated_users' => t('Associated users'),
              'operations'       => t('Operations'),
            ),
          'rows' => $rows,
        )
      );
    }
    else {
      return t('No Multi-Chado connection found!');
    }
  }

}
