<?php

/**
 * @file
 * Displays a list of available Chado connections.
 *
 * @ingroup tripal_mc
 */

$connection_table = $variables['connection_table'];
$pager            = $variables['pager'];
?>
<ul class="action-links">
  <li><?php echo l(t('Add connection'), 'tripal_mc_connection/add'); ?></li>
</ul>

<div class="tripal_mc-data-block-desc tripal-data-block-desc">
  This is the list of available Chado connection that can be assigned to users.
  In order to avoid unexpected behaviors, make sure that no more than one
  Chado connection is associated to a same Drupal user.
</div>
<br/>
<?php
if ($connection_table) {
  print $connection_table;
  print $pager;
}
else {
?>
  No Chado connection found! Would you like to <?php
  echo l(t('create a new connection'), 'tripal_mc_connection/add'); ?>?<br/>
  <br/>
<?php
}
?>
