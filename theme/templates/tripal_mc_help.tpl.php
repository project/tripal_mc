<?php

/**
 * @file
 * Displays global help about Tripal Multi-Chado module.
 *
 * @ingroup tripal_mc
 */
?>

<p>Tripal Multi-Chado Help Page</p>

<h3>Module Description:</h3>
<p>
  The Multi-Chado module enables the use of different Chado connections. By
  default, Tripal installs Chado in the PostgreSQL schema named 'chado' using
  Drupal PostgreSQL user account. The Multi-Chado module allows you to add new
  Chado connections that could use a different PostgreSQL user name than the
  one used by Drupal and/or use a different Chado schema name than the default
  one. New Chado connections can be associated to Drupal users so they will use
  their associated Chado connection rather than the default one when the will be
  connected to Drupal.
</p>

<h3>Setup Instructions:</h3>
<p>
Read the "Example Use-cases" section of the file README.md provided with this
module to have detailed instructions about how to setup your system for a given
use case.
</p>
<h4>PostgreSQL settings</h4>
<p>
  Let's assume you have a Drupal database with 3 schema in it: 'public' used by
  Drupal, 'chado' created by Tripal and 'chado_private' that contains Chado
  private data loaded from a dump.<br/>
  The PostgreSQL user account used by Drupal to access to your database is named
  'drupal'. This account is not allowed to access to your 'chado_private'
  schema:<br/>
  <code>REVOKE ALL PRIVILEGES ON SCHEMA chado_private FROM PUBLIC,drupal;<br/></code>
  You have a second PostgreSQL user account allowed to access to your private
  data named 'employee':<br/>
  <code>
    GRANT drupal TO employee; -- employee needs access to public schema<br/>
    GRANT ALL PRIVILEGES ON SCHEMA chado_private TO employee;<br/>
    GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA chado_private TO employee;<br/>
    GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA chado_private TO employee;<br/>
    ALTER DEFAULT PRIVILEGES IN SCHEMA chado_private GRANT ALL ON TABLES TO employee;<br/>
    ALTER DEFAULT PRIVILEGES IN SCHEMA chado_private GRANT ALL ON SEQUENCES TO employee;<br/>
  </code>
</p>
<h4>Drupal settings</h4>
<ol>
  <li>
    <?php echo l(t('Create 2 Drupal user accounts'), 'admin/people/create'); ?>,
    for instance 'priv' and 'priv_admin'
  </li>
  <li>
    <?php echo l(t('Create a new Chado connection'), 'tripal_mc_connection/add'); ?>
    that connects to the schema 'chado_private' using the PostgreSQL user
    account 'employee'.
  </li>
  <li>
    And associate the 2 Drupal users 'priv' and 'priv_admin' to this new Chado
    connection using the "Associated users" field
  </li>
  <li><?php echo l(t("Allow the user 'priv_admin' to manage Tripal Chado contents"), 'admin/people/permissions', array('fragment' => 'module-tripal_core')); ?></li>
  <li>
    Connect with the user 'priv_admin' and synchronize Tripal Chado contents
    like you usually do with your other Tripal admin account
  </li>
</ol>
<p>
Tripal will synchronize your private data with Drupal but will keep it private
inside Drupal.<br/>
Note: in order to also synchronize public data, you will need to use a different
admin account that is not associated with the private Chado Connection!
</p>
<p>
  Now, any Drupal user other than 'priv' and 'priv_admin' users will only see
  public data while when you connect with either of those 2 accounts, you will
  only see private data.
</p>

<h3>Features of this Module:</h3>
<p>
  By default, this module only supports a single specific Chado connection per
  Drupal user. While a same Chado connection could be shared across several
  users, it might be useful to also handle several Chado connection for a same
  user and let him or her or a module decide which of those connections to
  activate when needed. To achieve that, other extension modules can be created
  and use the hook <code><b>hook_tripal_mc_get_user_connection</b>(user)</code>.
  See the related documentation (section "API for developpers" in README.md
  and code documentation of "hook_tripal_mc_get_user_connection()") for details.
</p>
