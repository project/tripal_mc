<?php

/**
 * @file
 * This file contains all Drupal hooks for theming content.
 */

/**
 * Implements hook_preprocess_HOOK().
 *
 * Pre-rendre the table of available Chado connections.
 *
 * @ingroup tripal_mc
 */
function tripal_mc_preprocess_tripal_mc_admin(&$variables) {
  // Set the breadcrumb.
  $breadcrumb = array();
  $breadcrumb[] = l(t('Home'), '<front>');
  $breadcrumb[] = l(t('Administration'), 'admin');
  $breadcrumb[] = l(t('Tripal'), 'admin/tripal');
  $breadcrumb[] = l(t('Extensions'), 'admin/tripal/extension');
  $breadcrumb[] = l(t('Multi-Chado'), 'admin/tripal/extension/tripal_mc');
  drupal_set_breadcrumb($breadcrumb);

  $variables['connection_table'] = entity_ui_controller(TRIPAL_MC_CONNECTION_ENTITY_TYPE)->overviewTable();
  $variables['pager'] = theme('pager');
}
