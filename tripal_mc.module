<?php

/**
 * @file
 * Defines entry points of Tripal Multi-Chado module into Drupal/Tripal.
 */

require_once 'api/tripal_mc.const.inc';
require_once 'api/tripal_mc.api.php';
require_once 'api/tripal_mc_connection.api.php';
require_once 'includes/tripal_mc.admin.inc';
require_once 'includes/tripal_mc_connection.controller.inc';
require_once 'includes/tripal_mc_connection.inc';
require_once 'includes/tripal_mc_connection.pages.inc';
require_once 'theme/tripal_mc.theme.inc';

/**
 * Implements hook_boot().
 *
 * We need to implement this function in order to make Drupal load the module
 * and have our hooks enabled when Tripal will run SQL queries.
 *
 * @ingroup tripal_mc
 */
function tripal_mc_boot() {
}

/**
 * Implements hook_init().
 *
 * Override default Tripal synchronization process to support external Chado
 * schema.
 *
 * @ingroup tripal_mc
 */
function tripal_mc_init() {

  // Make sure we have at least Tripal default tables.
  $tripal_chado_tables = array_map(
    function ($bundle_name) {
      return 'chado_' . $bundle_name;
    },
    array_keys(field_info_bundles('TripalEntity'))
  );

  // Get list of synced tables.
  $sync_tables = variable_get(TRIPAL_MC_SYNC_TABLES_VAR);
  // Get list of tripal tables to sync by calling all modules implementing
  // hook_tripal_mc_sync_tables_alter.
  drupal_alter('tripal_mc_sync_tables', $sync_tables);
  // Make sure we got something we can work on: by default, work on Tripal
  // tables.
  if (!is_array($sync_tables)) {
    $sync_tables = $tripal_chado_tables;
  }

  foreach ($sync_tables as $sync_table => $sync_status) {
    // Setup Tripal sync functions for supported tables.
    // Here we check if the synchronized table names are valid to avoid
    // security issues.
    if (!function_exists($sync_table . '_chado_node_sync_form_submit')) {
      if ($sync_status && preg_match('/^\w+$/', $sync_table)) {
        // Implements hook_chado_node_sync_form_submit().
        // We dynamically implements those hooks using 'eval' because the tables
        // synchronized by Tripal MC is not static and may vary from an
        // execution to another.
        $function_creation =
          'function ' . $sync_table . '_chado_node_sync_form_submit($form, $form_state) {
            if (preg_match("/^Sync/", $form_state["values"]["op"])) {
              tripal_mc_sync_nodes($form, $form_state);
            }
          }';
        if (FALSE === eval($function_creation)) {
          drupal_set_message(
            t(
              '":sync_table_func" function could not be created. Tripal Multi-Chado will not be able to sync data!',
              array(':sync_table_func' => $sync_table . '_chado_node_sync_form_submit')
            ),
          'error'
          );
        }
      }
    }
    elseif (in_array($sync_table, $tripal_chado_tables)) {
      // Drupal default table already handled by an external module.
      drupal_set_message(
        t(
          '":sync_table_func" has already been defined by another module. Tripal Multi-Chado will not be able to sync data!',
          array(':sync_table_func' => $sync_table . '_chado_node_sync_form_submit')
        ),
        'error'
      );
    }
  }
}

/**
 * Implements hook_module_implements_alter().
 *
 * @ingroup tripal_mc
 */
function tripal_mc_module_implements_alter(&$implementations, $hook) {
  if ($hook == 'entity_load') {
    // Move tripal_mc_entity_load() to the end of the list because otherwise
    // we may remove entities before other module could process them and they
    // would try to process a NULL object.
    $tripal_mc_hook = $implementations['tripal_mc'];
    unset($implementations['tripal_mc']);
    $implementations['tripal_mc'] = $tripal_mc_hook;
  }
}

/**
 * Implements hook_permissions().
 *
 * Set the permission types that this module uses.
 *
 * @ingroup tripal_mc
 */
function tripal_mc_permission() {

  $permissions = array(
    'administer multi chado' => array(
      'title' => t('Administer Multi-Chado connections'),
      'description' => t('Allow to add, update and remove Chado connection settings and assign settings to users.'),
    ),
    'create tripal_mc_connection entities' => array(
      'title' => t('Create Chado Connection'),
      'description' => t('Allows users to create Chado Connection.'),
      'restrict access' => TRUE,
    ),
    'view tripal_mc_connection entities' => array(
      'title' => t('View Chado Connections'),
      'description' => t('Allows users to view Chado Connections.'),
      'restrict access' => TRUE,
    ),
    'edit any tripal_mc_connection entities' => array(
      'title' => t('Edit any Chado Connection'),
      'description' => t('Allows users to edit any Chado Connection.'),
      'restrict access' => TRUE,
    ),
    'edit own tripal_mc_connection entities' => array(
      'title' => t('Edit own Chado Connection'),
      'description' => t('Allows users to edit own Chado Connection.'),
      'restrict access' => TRUE,
    ),
  );

  return $permissions;
}

/**
 * Implements hook_node_access().
 *
 * Denies access to Chado nodes that are outside current Chado database schema.
 *
 * @ingroup tripal_mc
 */
function tripal_mc_node_access($node, $op, $account) {
  // Get node type.
  $type = is_string($node) ? $node : $node->type;
  // If object is not complete, load node to get the type.
  if (!$type && $node->nid) {
    $node = node_load($node->nid);
    $type = $node->type;
  }

  // Check if it's a Chado-synchronized node type.
  $sync_tables = tripal_mc_get_sync_tables();
  if (array_key_exists($type, $sync_tables)
      && $sync_tables[$type]) {
    if (!is_string($node) && $node->nid) {
      // Yes, check if node is part of current Chado connection.
      if (tripal_mc_connection_init()) {
        $connection_data = tripal_mc_get_user_connection($account);
        if ($connection_data) {
          // Using Multi-Chado nodes.
          $sql_query = "SELECT *
                          FROM {" . TRIPAL_MC_SYNC_TABLE_PREFIX . $type . "}
                          WHERE nid = :nid
                            AND " . TRIPAL_MC_CONNECTION_COLUMN . "
                              IN (:mccid);";
          $results = db_query(
            $sql_query,
            array(
              ':nid' => $node->nid,
              ':mccid' => $connection_data->mccid,
            )
          );
          $link = $results->rowCount();
          if (!$link) {
            return NODE_ACCESS_DENY;
          }
        }
        else {
          // Using Tripal nodes.
          $sql_query = "SELECT *
                          FROM {" . $type . "}
                          WHERE nid = :nid;";
          $results = db_query($sql_query, array(':nid' => $node->nid));
          $link = $results->rowCount();
          if (!$link) {
            return NODE_ACCESS_DENY;
          }
        }
      }
    }
    elseif (is_string($node) && function_exists('user_access')) {
      // On bootstrap, user module may not have been loaded yet.
      return user_access(
        'view ' . TRIPAL_MC_CONNECTION_ENTITY_TYPE . ' entities') ?
          NODE_ACCESS_IGNORE
          : NODE_ACCESS_DENY;
    }
  }

  // Returning nothing from this function would have the same effect.
  return NODE_ACCESS_IGNORE;
}

/**
 * Implements hook_entity_load().
 *
 * Removes nodes that are not related to current chado schema.
 *
 * @ingroup tripal_mc
 */
function tripal_mc_entity_load(&$entities, $type) {
  if ($entities && ('node' == $type)) {
    // Only gather nodes that are handled.
    $sync_tables = tripal_mc_get_sync_tables();
    $handled_nodes = array();
    foreach ($entities as $nid => $node) {
      if (array_key_exists($node->type, $sync_tables)) {
        $handled_nodes[$node->type][] = $nid;
      }
    }
    $nids_found = array();
    foreach ($handled_nodes as $chado_type => $nids) {
      $results = db_select($chado_type, 'tmcn')
        ->fields('tmcn', array('nid'))
        ->condition('nid', $nids, 'IN')
        ->execute();
      while ($record = $results->fetchAssoc()) {
        $nids_found[$record['nid']] = TRUE;
      }
    }
    foreach ($entities as $nid => $node) {
      if (array_key_exists($node->type, $sync_tables)) {
        if (!array_key_exists($nid, $nids_found)) {
          unset($entities[$nid]);
        }
      }
    }
    if (!$entities) {
      // Create a fake node to avoid errors.
      // +FIXME: we can't return an empty $entities array because during the
      // process, after this hook has been called, the system will call
      // implmentations of "hook_node_load" (like the "user_node_load" one)
      // by giving them an empty list of ids. Those hooks will issue SQL queries
      // with missing "nid" arguments that will crash Drupal.
      // In order to prevent this behavior, we could try to rewrite such queries
      // but they don't seem to be alterable using the hook hook_query_alter...
      // Another way of fixing this would be to intercept the data somewhere
      // before the rendering and remove the following fake node...
      // Maybe writing another node entity controller that would override the
      // default one could also be a possibility (more elegant).
      $entities[0] = (object) array(
        'nid'       => 0,
        'vid'       => 0,
        'type'      => $type,
        'language'  => LANGUAGE_NONE,
        'title'     => t('No content available.'),
        'uid'       => 0,
        'status'    => 0,
        'created'   => 0,
        'changed'   => time(),
        'comment'   => 0,
        'promote'   => 0,
        'sticky'    => 0,
        'tnid'      => 0,
        'translate' => 0,
        'name'      => '',
      );
    }
  }
}

/**
 * Implements hook_menu().
 *
 * Specifies menu items and URLs used by this module.
 *
 * @ingroup tripal_mc
 */
function tripal_mc_menu() {
  $items = array();

  // Administration menu.
  $items['admin/tripal/extension/tripal_mc'] = array(
    'title' => 'Tripal Multi-Chado',
    'description' => 'Administer Tripal Multi-Chado connections.',
    'page callback' => 'theme',
    'page arguments' => array('tripal_mc_admin'),
    'access arguments' => array('administer multi chado'),
    'type' => MENU_NORMAL_ITEM,
  );

  // Help from tripal_mc/theme/tripal_mc_help.tpl.php.
  $items['admin/tripal/extension/tripal_mc/help'] = array(
    'title' => 'Help',
    'description' => 'Basic Description of the Multi-Chado module.',
    'page callback' => 'theme',
    'page arguments' => array('tripal_mc_help'),
    'access arguments' => array('administer multi chado'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 10,
  );

  // Configuration page.
  $items['admin/tripal/extension/tripal_mc/configuration'] = array(
    'title' => 'Settings',
    'description' => 'Configure the Multi-Chado module.',
    'page callback' => 'theme',
    'page arguments' => array('tripal_mc_admin'),
    'access arguments' => array('administer multi chado'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => 5,
  );

  // Connection management.
  $items['tripal_mc_connection/add'] = array(
    'title' => 'Add Chado Connection',
    'description' => 'Add a new Chado connection.',
    'page callback' => 'tripal_mc_connection_admin_add_page',
    'access arguments' => array('administer multi chado'),
    'file' => 'includes/tripal_mc.admin.inc',
    'type' => MENU_LOCAL_TASK,
  );

  $mcc_uri = 'tripal_mc_connection/%tripal_mc_connection';
  $mcc_uri_argument_position = 1;

  $items[$mcc_uri] = array(
    'title callback' => 'entity_label',
    'title arguments' => array(
      'tripal_mc_connection', $mcc_uri_argument_position,
    ),
    'page callback' => 'tripal_mc_connection_view',
    'page arguments' => array($mcc_uri_argument_position),
    'access callback' => 'entity_access',
    'access arguments' => array(
      'view', 'tripal_mc_connection', $mcc_uri_argument_position,
    ),
    'file' => 'includes/tripal_mc_connection.pages.inc',
  );

  $items[$mcc_uri . '/view'] = array(
    'title' => 'View',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );

  $items[$mcc_uri . '/delete'] = array(
    'title' => 'Delete',
    'page callback' => 'drupal_get_form',
    'page arguments' => array(
      'tripal_mc_connection_delete_form', $mcc_uri_argument_position,
    ),
    'access callback' => 'entity_access',
    'access arguments' => array(
      'edit', 'tripal_mc_connection', $mcc_uri_argument_position,
    ),
    'file' => 'includes/tripal_mc.admin.inc',
  );

  $items[$mcc_uri . '/edit'] = array(
    'title' => 'Edit',
    'page callback' => 'drupal_get_form',
    'page arguments' => array(
      'tripal_mc_connection_form', $mcc_uri_argument_position,
    ),
    'access callback' => 'entity_access',
    'access arguments' => array(
      'edit', 'tripal_mc_connection', $mcc_uri_argument_position,
    ),
    'file' => 'includes/tripal_mc.admin.inc',
    'type' => MENU_LOCAL_TASK,
    'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
  );

  return $items;
}

/**
 * Implements hook_theme().
 *
 * Setup theme functions supported by the module.
 *
 * @ingroup tripal_mc
 */
function tripal_mc_theme($existing, $type, $theme, $path) {
  $items = array(
    // The base template.
    'tripal_mc_admin' => array(
      'variables' => array('connection_table' => NULL),
      'template'  => 'tripal_mc_admin',
      'path'      => "$path/theme/templates",
    ),
    // The help template.
    'tripal_mc_help' => array(
      'template'  => 'tripal_mc_help',
      'variables' => array(NULL),
      'path'      => "$path/theme/templates",
    ),
  );

  return $items;
}

/**
 * Implements hook_help().
 *
 * Adds a help page to the module list.
 *
 * @ingroup tripal_mc
 */
function tripal_mc_help($path, $arg) {
  if ($path == 'admin/help#tripal_mc') {
    return theme('tripal_mc_help', array());
  }
}

/**
 * Implements hook_form_alter().
 *
 * Disables unsupported Chado connection elements.
 * Changes the behavior of Tripal Chado record synchronization form.
 *
 * @ingroup tripal_mc
 */
function tripal_mc_form_alter(&$form, &$form_state, $form_id) {
  if ('tripal_mc_connection_form' == $form_id) {
    // Disable unsupported elements.
    $form['host']['#disabled']     = TRUE;
    $form['port']['#disabled']     = TRUE;
    $form['database']['#disabled'] = TRUE;
  }
  elseif ('chado_node_sync_form' == $form_id) {

    // Disable submit button.
    $form['sync']['method'] = array(
      '#type' => 'radios',
      '#title' => t('Synchronization method'),
      '#default_value' => 'batch',
      '#options' => array(
        'batch' => t('use batch process'),
        'cron' => t('use Tripal cron'),
      ),
      '#description' => t('Batch process requires your browser to remain on the page while the synchronization process is running therefore it is not recommanded when you have a lot of records to synchronize. Tripal cron uses Drush and can be launched manually from the command line or by a cron process therefore records synchronization may be delayed. Tripal cron is recommanded for large amount of records to synchronize.'),
    );

    // Add a checkbox for orphans clean up.
    $form['sync']['orphans'] = array(
      '#type' => 'checkbox',
      '#title' => t('Clean up orphans as well'),
      '#description' => t('Also clean up orphans when synchronizing.'),
    );

    // Disable regular orphan clean up form.
    $form['cleanup']['button']['#disabled'] = TRUE;
    $form['cleanup']['button']['#value'] =
      $form['cleanup']['button']['#value']
      . ' '
      . t('- Disabled by Tripal Multi-Chado');
  }
}

/**
 * Implements hook_views_pre_view().
 *
 * Changes the default database used by views to make them use the user-specific
 * one instead if there is one.
 *
 * @ingroup tripal_mc
 */
function tripal_mc_views_pre_view(&$view, &$display_id, &$args) {
  // Check if current used has a specific chado connection.
  $connection_data = tripal_mc_get_user_connection();
  if ($connection_data && tripal_mc_connection_init()) {
    // Yes, get the connection name.
    $connection_key = tripal_mc_get_connection_key();
    // If we got a name and the view did not use a specific database then
    // change the database used by the view.
    if ($connection_key && !$view->base_database) {
      $view->base_database = $connection_key;
    }
  }
}

/**
 * Implements hook_entity_info().
 *
 * Adds Chado connection entity type.
 *
 * @ingroup tripal_mc
 */
function tripal_mc_entity_info() {
  return array(
    TRIPAL_MC_CONNECTION_ENTITY_TYPE => array(
      'label'            => t('Multi-Chado connection'),
      'plural label'     => t('Multi-Chado connections'),
      'entity class'     => 'TripalMCConnection',
      'controller class' => 'TripalMCConnectionController',
      'base table'       => TRIPAL_MC_CONNECTION_TABLE,
      'static cache'     => TRUE,
      'field cache'      => TRUE,
      'load hook'        => 'tripal_mc_connection_load',
      'uri callback'     => 'entity_class_uri',
      'label callback'   => 'entity_class_label',
      'entity keys'      => array(
        'id'    => 'mccid',
        'name'  => 'machine_name',
        'label' => 'title',
      ),
      'fieldable'        => TRUE,
      'exportable'       => TRUE,
      // Use the default label() and uri() functions.
      'admin ui'         => array(
        'path'             => 'admin/content/tripal_mc_connection',
        'controller class' => 'TripalMCConnectionUIController',
        'file'             => 'includes/tripal_mc_connection.ui_controller.inc',
      ),
      'access callback'  => 'tripal_mc_connection_access',
      'module'           => 'tripal_mc',
      'view modes'       => array(
        'default' => array(
          'label'           => t('Chado Connection details'),
        ),
        'full'    => array(
          'label'           => t('Full Chado Connection details'),
          'custom settings' => FALSE,
        ),
      ),
    ),
  );
}

/**
 * Implements hook_entity_property_info_alter().
 *
 * Add multiple-user association to Chado connections.
 *
 * @ingroup tripal_mc
 */
function tripal_mc_connection_entity_property_info_alter(&$info) {
  $properties = &$info[TRIPAL_MC_CONNECTION_ENTITY_TYPE]['properties'];
  if (module_exists('user')) {
    $properties['uid'] = array(
      'label'             => t('Author'),
      'type'              => 'user',
      'description'       => t('The author of the Chado connection.'),
      'setter callback'   => 'entity_property_verbatim_set',
      'setter permission' => 'administer multi chado',
      'required'          => TRUE,
      'schema field'      => 'uid',
    );
  }
}

/**
 * Implements hook_page_title().
 *
 * Returns Chado connection title.
 *
 * @ingroup tripal_mc
 */
function tripal_mc_connection_page_title($multi_chado_connection) {
  return $multi_chado_connection->mccid;
}

/**
 * Implements hook_block_info().
 *
 * This hook declares the block that shows current Chado schema.
 *
 * @ingroup tripal_mc
 */
function tripal_mc_block_info() {
  $blocks['tripal_mc_current_schema'] = array(
    'info' => t('Chado schema'),
    'cache' => DRUPAL_CACHE_PER_ROLE,
  );

  return $blocks;
}

/**
 * Implements hook_block_view().
 *
 * Shows the current Chado schema name.
 *
 * @ingroup tripal_mc
 */
function tripal_mc_block_view($delta = '') {
  switch ($delta) {
    case 'tripal_mc_current_schema':
      $block['subject'] = t('Active Chado Schema');
      $block['content'] = tripal_get_schema_name('chado');
      break;
  }
  return $block;
}

/**
 * Implements hook_chado_connection_alter().
 *
 * If current user has a specific Chado connection associated to his account,
 * we make Tripal queries use it.
 *
 * @ingroup tripal_mc
 */
function tripal_mc_chado_connection_alter(&$settings) {
  // User has a specific connection?
  $connection_data = tripal_mc_get_user_connection();
  if ($connection_data && tripal_mc_connection_init()) {
    // Yes, do we want to connect to Chado?
    if ('chado' == $settings['dbname']) {
      // Alter Chado connection settings in case schema name is not 'chado'.
      $settings['new_active_db']   = $connection_data->schemaname;
      $settings['new_search_path'] =
        "pg_temp,"
        . $connection_data->schemaname
        . ","
        . tripal_get_schema_name('drupal');
    }
  }
}

/**
 * Implements hook_tripal_get_schema_name_alter().
 *
 * Returns current Chado schema name.
 *
 * @ingroup tripal_mc
 */
function tripal_mc_tripal_get_schema_name_alter(&$schema_name, $context) {
  if ($context
      && array_key_exists('schema', $context)) {
    if ($context['schema'] == 'chado') {
      // User has a specific connection?
      $connection_data = tripal_mc_get_user_connection();
      if ($connection_data && tripal_mc_connection_init()) {
        $schema_name = $connection_data->schemaname;
      }
    }
  }
}

/**
 * Implements hook_chado_query_alter().
 *
 * Removes the 'pulbic.' qualifier from supported sync tables to make Postgre
 * use the temporary views instead.
 *
 * @ingroup tripal_mc
 */
function tripal_mc_chado_query_alter(&$sql, &$args) {
  // Make sure view-overridden tables are not accessed directly:
  // we remove 'public.' table qualifiers in order to make sure Postgre
  // will check pg_temp namespace first (where the temporary views overriding
  // the tables are hold) before accessing an overridden synchronized table.
  $sync_tables = tripal_mc_get_sync_tables();
  $table_regex = '/(\W)public\.(' . implode('|', $sync_tables) . ')(\W)/';
  $sql = preg_replace($table_regex, '\1\2\3', $sql);
}

/**
 * Implements hook_tripal_mc_sync_tables_alter().
 *
 * By default, we want to support all Chado tables synchronized by Tripal.
 *
 * @see tripal_mc_get_sync_tables()
 *
 * @ingroup tripal_mc
 */
function tripal_mc_tripal_mc_sync_tables_alter(&$sync_tables) {
  // Get Tripal 3 entitites.
  $tripal_chado_bundles = array_keys(field_info_bundles('TripalEntity'));

  foreach ($tripal_chado_bundles as $bundle_name) {
    $chado_entity_table = 'chado_' . $bundle_name;
    $sync_tables[$chado_entity_table] = TRUE;
  }
}

/**
 * Implements hook_token_info().
 *
 * @ingroup tripal_mc
 */
function tripal_mc_token_info() {

  // Default tokens are provided by the entity module.
  $type = array(
    'name' => t('Multi-Chado connection'),
    'description' => t('Tokens related to Multi-Chado connections.'),
    'needs-data' => 'tripal_mc_connection',
  );

  // Additional tokens for Multi-Chado connection.
  $tripal_mc_connection['edit-url'] = array(
    'name' => t("Edit URL"),
    'description' => t("The URL of the Multi-Chado connection's edit page."),
  );

  $tripal_mc_connection['owner'] = array(
    'name' => t("Owner"),
    'description' => t("The user who created the Multi-Chado connection."),
    'type' => 'user',
  );

  return array(
    'types' => array('tripal_mc_connection' => $type),
    'tokens' => array('tripal_mc_connection' => $tripal_mc_connection),
  );
}

/**
 * Implements hook_tokens().
 *
 * @ingroup tripal_mc
 */
function tripal_mc_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $url_options = array('absolute' => TRUE);
  if (isset($options['language'])) {
    $url_options['language'] = $options['language'];
  }
  $sanitize = !empty($options['sanitize']);

  $replacements = array();

  if ($type == 'tripal_mc_connection'
      && !empty($data['tripal_mc_connection'])) {
    $connection = $data['tripal_mc_connection'];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'edit-url':
          $replacements[$original] = url('tripal_mc_connection/' . $connection->mccid . '/edit', $url_options);
          break;

        case 'owner':
          $user = user_load($connection->uid);
          $replacements[$original] = $sanitize ? filter_xss($user->name) : $user->name;
          break;

        case 'password':
          $replacements[$original] = '-';
          break;
      }
    }

    if ($owner_tokens = token_find_with_prefix($tokens, 'owner')) {
      $owner = user_load($connection->uid);
      $replacements += token_generate('user', $owner_tokens, array('user' => $owner), $options);
    }

  }

  return $replacements;
}
