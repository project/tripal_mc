<?php

/**
 * @file
 * Defines Mutli-Chado API functions.
 */

require_once 'tripal_mc.const.inc';

/**
 * Global initialization status.
 *
 * Internal uses only.
 *
 * @var $_tripal_mc_init_error
 */
$_tripal_mc_init_error = NULL;

/**
 * Global current connection identifier.
 *
 * Internal uses only.
 * Use tripal_mc_get_connection_id() to get the connection id value.
 *
 * @var $_tripal_mc_connection_key
 */
$_tripal_mc_connection_key = NULL;

/**
 * Global current user for Tripal Multi-Chado.
 *
 * Internal uses only.
 *
 * @var $_tripal_mc_user
 */
$_tripal_mc_user = NULL;

/**
 * Initialize and activate user-specific Chado connection if one exists.
 *
 * Returns TRUE if the initialization was done successfully, FALSE otherwise.
 *
 * @ingroup tripal_mc
 */
function tripal_mc_connection_init() {
  global $_tripal_mc_init_error;

  if (isset($_tripal_mc_init_error)) {
    return !$_tripal_mc_init_error;
  }

  $_tripal_mc_init_error = FALSE;

  // User has a specific connection?
  $connection_data = tripal_mc_get_user_connection();
  if ($connection_data) {
    // Get default connection for server, port and database name as we currently
    // only support different schema in a same database.
    // We will use those server, port and database name rather than the provided
    // ones.
    $current_connection_info = Database::getConnectionInfo();

    // Prepare database connection settings.
    $new_database = array(
      'database' => $current_connection_info['default']['database'],
      'username' => $connection_data->username,
      'password' => $connection_data->password,
      'host'     => $current_connection_info['default']['host'],
      'port'     => $current_connection_info['default']['port'],
      'driver'   => 'pgsql',
      'prefix'   => '',
      // Add schema (not handled by Drupal/Tripal).
      'chado_schema'   => $connection_data->schemaname,
    );
    $connection_key = tripal_mc_get_connection_key();

    try {
      // Add user database connection on-the-fly.
      Database::addConnectionInfo($connection_key, 'default', $new_database);
      // And activate that connection.
      db_set_active($connection_key);
      // Make sure schema exists.
      $schema_test_result =
        db_query(
          'SELECT schema_name FROM {information_schema.schemata} WHERE schema_name = :schema_name;',
          array(':schema_name' => $connection_data->schemaname)
        );
      if ($schema_test_result->rowCount()) {
        // Set new search path to make sure the default chado schema will be
        // replaced by the specified one (nb. which may or may not still be
        // 'chado').
        $schema_name = tripal_get_schema_name('public');
        if (!preg_match('/^\w+$/', $schema_name)) {
          throw new Exception(t(
            "Invalid schema name ':schema_name'!",
            array(':schema_name' => $schema_name)
          ));
        }
        db_query(
          'SET search_path TO pg_temp,'
          . $schema_name
          . ','
          . $connection_data->schemaname
          . ';'
        );

        // Initialize virtual tables if needed.
        tripal_mc_init_virtual_tables();
      }

      // Invoke rule.
      if (module_exists('rules')) {
        rules_invoke_event('tripal_mc_active_connection', $connection_data);
      }
    }
    catch (Exception $e) {
      // Rollback to default connection.
      db_set_active('default');
      drupal_set_message(
        t(
          'Failed to use user connection: :message',
          array(':message' => $e->getMessage())
        ),
        'error'
      );
      $_tripal_mc_init_error = TRUE;
    }
  }

  return !$_tripal_mc_init_error;
}

/**
 * Restore 'genetic_code', 'frange', 'so' schemata ownership after new install.
 *
 * After install, if we used a specific connection, we need to reassign the
 * ownership of the schema genetic_code, frange and so to the default Drupal
 * postgreSQL user.
 * Otherwise, the default user would not be able to access those schema anymore
 * and another installation with another database user would fail.
 *
 * @todo to call after a new Chado install.
 *
 * @ingroup tripal_mc
 */
function tripal_mc_fix_schema_ownership() {
  // Do we use a user-specific connection?
  $connection_data = tripal_mc_get_user_connection();
  if ($connection_data) {
    // Not using default connection, reassign ownership.
    $default_connection_info = Database::getConnectionInfo();
    $default_db_user = $default_connection_info['default']['username'];
    $sql_query = "ALTER SCHEMA :schema_name OWNER TO :db_user;";
    foreach (array('genetic_code', 'frange', 'so') as $schema_name) {
      db_query($sql_query,
        array(
          ':db_user' => $default_db_user,
          ':schema_name' => $schema_name,
        )
      );
    }
  }
}

/**
 * Create temporary views that override Tripal Chado-synchronized tables.
 *
 * Those views will act as virtual tables during the database connection and
 * will disappear once the database connection is closed.
 *
 * Since this function is called after we activated any user-specific
 * connection, we ensure that those views won't be shared across users as they
 * will be created in the scope of the user-specific connection.
 *
 * Tripal will manipulate those views just like it would with its chado_* tables
 * but beside the scene, those manipulation will be ported on tripal_mc_chado_*
 * tables instead. Doing so, we keep external Chado informations outside of
 * Tripal scope in case the Multi-Chado module is disabled but not uninstalled.
 *
 * If other Tripal extensions implements Chado data types that need to be
 * synchronized the same way that default Chado types are, then those extensions
 * should implement hook_tripal_mc_sync_tables_alter() in order to let the
 * Multi-Chado module handle them as well.
 *
 * @see hook_tripal_mc_sync_tables_alter()
 *
 * @ingroup tripal_mc
 */
function tripal_mc_init_virtual_tables() {
  // We try the first query and if it fails, it means the work has already been
  // done and we can continue without worries.
  try {
    $connection_data = tripal_mc_get_user_connection();

    // Get schema structure.
    $schema = drupal_get_schema();

    // If not initialized, call hook_tripal_mc_sync_tables to get the list of
    // synchronized tables.
    $sync_tables = tripal_mc_get_sync_tables();

    // Override each table by a temporary view.
    foreach ($sync_tables as $sync_table => $sync_enabled) {
      if ($sync_enabled
        && array_key_exists($sync_table, $schema)
        && preg_match('/^[\w\.]+$/', $sync_table)
      ) {
        $fields = array_filter(
          array_keys($schema[$sync_table]['fields']),
          function ($k) {
            return TRIPAL_MC_CONNECTION_COLUMN != $k;
          }
        );
        try {
          $mc_sync_table = TRIPAL_MC_SYNC_TABLE_PREFIX . $sync_table;
          // Create the overriding view.
          db_query(
            "CREATE OR REPLACE TEMPORARY VIEW " . $sync_table
              . " AS SELECT "
              . implode(', ', $fields)
              . " FROM "
              . $mc_sync_table
              . " WHERE "
              . TRIPAL_MC_CONNECTION_COLUMN . " = :mccid;",
            array(':mccid' => $connection_data->mccid)
          );

          // Add insert rule.
          $insert_rule =
            "CREATE OR REPLACE RULE " . $mc_sync_table . "_i AS ON INSERT TO "
            . $sync_table
            . " DO INSTEAD INSERT INTO " . $mc_sync_table
            . " (" . implode(', ', $fields) . ", " . TRIPAL_MC_CONNECTION_COLUMN
            . ") VALUES (";
          foreach ($fields as $field) {
            // For columns with default values use
            // COALESCE(NEW.xxx, table default value).
            if (
              array_key_exists(
                'default',
                $schema[$sync_table]['fields'][$field]
              )
            ) {
              // Just in case, for serial default values, we should use
              // COALESCE(NEW.xxx, NEXTVAL(''sequence_name'')).
              $insert_rule .=
                " COALESCE(NEW.$field, '"
                . $schema[$sync_table]['fields'][$field]['default']
                . "'), ";
            }
            else {
              $insert_rule .= " NEW.$field, ";
            }
          }
          $insert_rule .= " " . $connection_data->mccid . ");";
          db_query($insert_rule);

          // Add update rule.
          $update_rule =
            "CREATE OR REPLACE RULE " . $mc_sync_table . "_u AS ON UPDATE TO "
            . $sync_table
            . " DO INSTEAD UPDATE " . $mc_sync_table . " SET ";
          foreach ($fields as $field) {
            $update_rule .= " $field = NEW.$field, ";
          }
          $update_rule .= ' '
            . TRIPAL_MC_CONNECTION_COLUMN
            . " = "
            . $connection_data->mccid
            . " WHERE ";
          // Use primary key(s).
          $where_conditions = array();
          foreach ($schema[$sync_table]['primary key'] as $primary_key) {
            $where_conditions[] = $primary_key . ' = OLD.' . $primary_key;
          }
          $update_rule .= implode(" AND ", $where_conditions);
          $update_rule .= ";";
          db_query($update_rule);

          // Add delete rule.
          $delete_rule =
            "CREATE OR REPLACE RULE " . $mc_sync_table . "_d AS ON DELETE TO "
            . $sync_table
            . " DO INSTEAD DELETE FROM " . $mc_sync_table
            . " WHERE "
            . TRIPAL_MC_CONNECTION_COLUMN . " = " . $connection_data->mccid;
          // Use primary key(s).
          foreach ($schema[$sync_table]['primary key'] as $primary_key) {
            $delete_rule .= " AND " . $primary_key . " = OLD." . $primary_key;
          }
          $delete_rule .= ";";
          db_query($delete_rule);
        }
        catch (PDOException $e) {
          drupal_set_message(
            t(
              "Tripal Multi-Chado: failed to mask :sync_table table!\n:message",
              array(
                ':sync_table' => $sync_table,
                ':message' => $e->getMessage(),
              )
            ),
            'error'
          );
        }
      }
    }
  }
  catch (PDOException $e) {
    drupal_set_message(
      t(
        "Tripal Multi-Chado: failed to initialize virtual tables!\n:message",
        array(':message' => $e->getMessage())
      ),
      'warning'
    );
  }
}

/**
 * Adds tables to Tripal MC synchronised table list.
 *
 * This example shows how to let the Multi-Chado module also handle the
 * chado_nd_experiment linking table.
 *
 * Note: modules that want to prevent the Multi-Chado module from using its
 * synchronized data rather than the default one provided by Tripal should not
 * remove the corresponding keys but rather set their values to FALSE.
 * If a key is removed, the Multi-Chado may not remove the appropriate data
 * when a connection is removed or the module is uninstalled.
 *
 * @param array $sync_tables
 *   An associative array where keys are PostgreSQL table names from the Drupal
 *   (public) schema that are used to link Chado records with Drupal nodes. If
 *   the associated value of a key is set to FALSE, the Multi-Chado will just
 *   ignore the related table.
 *
 * @ingroup tripal_mc
 */
function hook_tripal_mc_sync_tables_alter(array &$sync_tables) {
  $sync_tables['chado_nd_experiment'] = TRUE;
}

/**
 * Returns the list of synchronized tables.
 *
 * Returns the list of synchronized tables currently supported by the
 * Multi-Chado module in an array of synchronized table names as keys and
 * their support status as value. If a value is false, it means the Multi-Chado
 * module will not support the table and will keep using the Tripal one.
 *
 * @return array
 *   An array of table names (strings).
 *
 * @ingroup tripal_mc
 */
function tripal_mc_get_sync_tables() {
  $sync_tables = variable_get(TRIPAL_MC_SYNC_TABLES_VAR, array());
  drupal_alter('tripal_mc_sync_tables', $sync_tables);
  return $sync_tables;
}

/**
 * Returns the user-specific Chado connection if one exists.
 *
 * Extensions that would like to provide a user interface to select a Chado
 * connection to activate should implement the hook
 * hook_tripal_mc_user_connection_alter().
 *
 * @param object $tripal_mc_user
 *   A Drupal user object or NULL (current user will be used).
 *
 * @return object
 *   Returns the Tripal Multi-Chado connection currenlty active associated with
 *   the given user (or current user if unspecified). Returns NULL if no
 *   specific connections have been found.
 *
 * @see hook_tripal_mc_user_connection_alter()
 *
 * @ingroup tripal_mc
 */
function tripal_mc_get_user_connection($tripal_mc_user = NULL) {
  global $_tripal_mc_user;
  global $user;

  // Use global Multi-Chado user as default.
  if (!isset($tripal_mc_user)) {
    $tripal_mc_user = $_tripal_mc_user;
  }

  if (!isset($tripal_mc_user)) {
    // Check for drush bootstrap and load the appropriate user if needed.
    if (function_exists('drush_main')) {
      module_load_include('inc', 'tripal_core', 'tripal_core.drush');

      // We are using drush with a specified user (either from drush or Tripal).
      $drush_user = drush_get_option('user');
      $uname = drush_get_option('username');
      if (isset($drush_user) || isset($uname)) {
        if ($drush_user and is_numeric($drush_user)) {
        }
        elseif ($drush_user) {
          $username = $drush_user;
        }
        if ($uname) {
          $username = $uname;
        }
        // Use Tripal function to load user.
        if (!empty($username)) {
          drush_tripal_core_set_user($username);
        }
      }
    }
    $tripal_mc_user = $_tripal_mc_user = $user;
  }

  // Get default connctions associated with user.
  $connections = tripal_mc_get_user_available_connections($tripal_mc_user);

  // Only keep the first connection of the list.
  $connection = reset($connections);

  $tripal_mc_user_clone = clone $tripal_mc_user;
  drupal_alter('tripal_mc_user_connection', $connection, $tripal_mc_user_clone);

  return $connection;
}

/**
 * Returns the list of user-specific Chado connections.
 *
 * @param object $tripal_mc_user
 *   A Drupal user object or NULL (current user will be used).
 *
 * @return array
 *   Returns an array of Tripal Multi-Chado connection objects associated with
 *   the given user (or current user if unspecified) indexed by their mccids.
 *   The array can be empty.
 *
 * @see hook_tripal_mc_user_connections_alter()
 *
 * @ingroup tripal_mc
 */
function tripal_mc_get_user_available_connections($tripal_mc_user = NULL) {
  global $_tripal_mc_user;
  global $user;

  // Use global Multi-Chado user as default.
  if (!isset($tripal_mc_user)) {
    $tripal_mc_user = $_tripal_mc_user;
  }

  if (!isset($tripal_mc_user)) {
    // Check for drush bootstrap and load the appropriate user if needed.
    if (function_exists('drush_main')) {
      module_load_include('inc', 'tripal_core', 'tripal_core.drush');

      // We are using drush with a specified user (either from drush or Tripal).
      $drush_user = drush_get_option('user');
      $uname = drush_get_option('username');
      if (isset($drush_user) || isset($uname)) {
        if ($drush_user and is_numeric($drush_user)) {
        }
        elseif ($drush_user) {
          $username = $drush_user;
        }
        if ($uname) {
          $username = $uname;
        }
        // Use Tripal function to load user.
        if (!empty($username)) {
          drush_tripal_core_set_user($username);
        }
      }
    }
    $tripal_mc_user = $_tripal_mc_user = $user;
  }

  // Get connections associated with user.
  $connections = array();
  if (function_exists('user_access')) {
    // Try to find a connection associated to current user.
    try {
      // Make sure all is loaded.
      $query = new EntityFieldQuery();
      $query->entityCondition(
        'entity_type',
        TRIPAL_MC_CONNECTION_ENTITY_TYPE
      )->fieldCondition(
        TRIPAL_MC_ASSOCIATED_USER_FIELD,
        'target_id',
        $tripal_mc_user->uid
      );
      $result = $query->execute();
    }
    catch (Exception $e) {
      drupal_set_message(
        t(
          'Failed to fetch user connection: :message',
          array(':message' => $e->getMessage())
        ),
        'error'
      );
    }
    // User has a specific connection?
    if (!empty($result)) {
      // Yes, load user Chado connection settings.
      $connection_ids = array_keys($result[TRIPAL_MC_CONNECTION_ENTITY_TYPE]);
      $connections = entity_load(
        TRIPAL_MC_CONNECTION_ENTITY_TYPE,
        $connection_ids
      );
    }
  }
  else {
    try {
      // User module not loaded yet and will generate a
      // "Call to undefined function user_access()" error
      // use the "raw way" to load the connection.
      // Note: if a user is associated to a connection, it means he/she is
      // allowed to use that connection so we should not bother about
      // user_access not being used in that precise case (loading).
      $sql_query = "SELECT c.* FROM {" . TRIPAL_MC_CONNECTION_TABLE
        . "} c  JOIN {field_data_" . TRIPAL_MC_ASSOCIATED_USER_FIELD
        . "} u    ON (u.entity_id = c.mccid) WHERE u."
        . TRIPAL_MC_ASSOCIATED_USER_FIELD . "_target_id = :uid;";
      $result = db_query($sql_query, array(':uid' => $tripal_mc_user->uid));
      while ($chado_connection = $result->fetchObject()) {
        $chado_connection->password = tripal_mc_decrypt_password(
          $chado_connection->password
        );
        $chado_connection->password_decrypted = TRUE;
        $connections[$chado_connection->mccid] = $chado_connection;
      }
    }
    catch (Exception $e) {
      drupal_set_message(
        t(
          'Failed to fetch user connection (raw way): :message',
          array(':message' => $e->getMessage())
        ),
        'error'
      );
    }
  }

  $tripal_mc_user_clone = clone $tripal_mc_user;
  drupal_alter('tripal_mc_user_connections', $connections, $tripal_mc_user_clone);

  // Returns all the available connections for the given user.
  return $connections;
}

/**
 * Allows an extension module to select the Chado connection to activate.
 *
 * This example demonstrate how we could associate a connection to a value in
 * the global $_SESSION array.
 *
 * @param object $connection
 *   A Tripal Multi-Chado connection object representing current connection to
 *   activate.
 * @param object $tripal_mc_user
 *   The user for which the connection has been requested.
 *   Should be a Drupal user object or NULL. This object won't be altered but
 *   might be taken into account by other implementations of the hook.
 *
 * @ingroup tripal_mc
 */
function hook_tripal_mc_user_connection_alter(&$connection, &$tripal_mc_user) {
  global $user;

  // Get connections associated with current user.
  $connections = tripal_mc_get_user_available_connections($tripal_mc_user);

  // Not current user, do nothing: we only handle current user.
  // Not set in session, do nothing.
  // Otherwise, get the matching connection if one.
  if (($user->uid == $tripal_mc_user->uid)
      && isset($_SESSION['tripal_mc_connection'])) {
    if (array_key_exists($_SESSION['tripal_mc_connection'], $connections)) {
      $connection = $connections[$_SESSION['tripal_mc_connection']];
    }
    elseif (0 == $_SESSION['tripal_mc_connection']) {
      // Use default site connection.
      $connection = NULL;
    }
  }
}

/**
 * Allows an extension module to alter users Chado connections.
 *
 * This example demonstrate how we could add a connections associated to user 2
 * to any other user.
 *
 * @param array $connections
 *   An array of Tripal Multi-Chado connection objects available for the given
 *   user indexed by their mccids. The array might be empty but not NULL.
 * @param object $tripal_mc_user
 *   The user for which the connections have been requested.
 *   Should be a Drupal user object or NULL. This object won't be altered but
 *   might be taken into account by other implementations of the hook.
 *
 * @ingroup tripal_mc
 */
function hook_tripal_mc_user_connections_alter(array &$connections, &$tripal_mc_user) {
  $special_user_2 = user_load(2);
  if ($special_user_2 && (2 != $tripal_mc_user->uid)) {
    $new_connections = tripal_mc_get_user_available_connections($special_user_2);
    $connections = array_merge($connections, $new_connections);
  }
}

/**
 * Returns user-specific Chado connection name (database key).
 *
 * @ingroup tripal_mc
 */
function tripal_mc_get_connection_key() {
  global $_tripal_mc_connection_key;

  // Check if we already initialized the connection.
  if (!isset($_tripal_mc_connection_key)) {
    // Get session ID as connection key identifier.
    $session_id = session_id();
    if ('' === $session_id) {
      // If no session has been returned, then use a random key.
      $session_id = uniqid();
    }
    $_tripal_mc_connection_key = 'tripal_mc_' . $session_id;
  }

  return $_tripal_mc_connection_key;
}

/**
 * Helper function used to synchronize Tripal nodes with Chado data.
 *
 * Depending on the form settings, it will run node synchronization as a Drupal
 * batch process or a Tripal job.
 *
 * @param array $form
 *   A Drupal form structure (not used).
 * @param array $form_state
 *   A Drupal form state structure.
 */
function tripal_mc_sync_nodes(array $form, array $form_state) {
  global $user;

  // Get arguments.
  $args = $form_state['chado_node_api'];
  $module = $form_state['chado_node_api']['hook_prefix'];
  $base_table = $form_state['chado_node_api']['base_table'];
  $linking_table = $form_state['values']['linking_table'];
  $node_type = $form_state['values']['node_type'];
  $method = $form_state['values']['method'];
  $sync_orphans = $form_state['values']['orphans'];

  // Get the types separated into a consistent string.
  $types = array();
  if (isset($form_state['values']['type_ids'])) {
    // Separate by new line or comma.
    $temp_types = preg_split("/[,\n\r]+/", $form_state['values']['type_ids']);

    // Remove any extra spacing around the types.
    for ($i = 0; $i < count($temp_types); ++$i) {
      // Skip empty types.
      if (trim($temp_types[$i]) == '') {
        continue;
      }
      $types[$i] = trim($temp_types[$i]);
    }
  }

  // Get the ids to be synced.
  $ids = array();
  if (array_key_exists('ids', $form_state['values'])) {
    foreach ($form_state['values']['ids'] as $id => $selected) {
      if ($selected) {
        $ids[] = $id;
      }
    }
  }

  // Get the organism to be synced.
  $organism_id = FALSE;
  if (array_key_exists('organism_id', $form_state['values'])) {
    $organism_id = $form_state['values']['organism_id'];
  }

  // Job Arguments.
  $sync_job_args = array(
    'base_table' => $base_table,
    'max_sync' => (!empty($form_state['values']['max_sync'])) ? $form_state['values']['max_sync'] : FALSE,
    'organism_id' => $organism_id,
    'types' => $types,
    'ids' => $ids,
    'linking_table' => $linking_table,
    'node_type' => $node_type,
  );
  $orphan_job_args = array(
    'table' => $base_table,
    'nentries' => (!empty($form_state['values']['cleanup_batch_size'])) ? $form_state['values']['cleanup_batch_size'] : 25000,
    'linking_table' => $linking_table,
    'node_type' => $node_type,
  );

  $title = t(
    "Sync %type",
    array('%type' => $args['record_type_title']['plural'])
  );

  if ('cron' == $method) {
    tripal_add_job(
      $title,
      $module,
      'chado_node_sync_records',
      $sync_job_args,
      $user->uid
    );
    if ($sync_orphans) {
      tripal_add_job(
        $title,
        $module,
        'tripal_mc_cleanup_orphaned_nodes',
        $orphan_job_args,
        $user->uid
      );
    }
  }
  elseif ('batch' == $method) {
    $batch = array(
      'title' => $title,
      'operations' => array(
        array('tripal_mc_run_node_sync', array($sync_job_args)),
      ),
      'finished' => 'tripal_mc_sync_finished',
    );
    if ($sync_orphans) {
      $batch['operations'][] = array(
        'tripal_mc_run_orphan_sync',
        array($orphan_job_args),
      );
    }
    batch_set($batch);
  }
  else {
    drupal_set_message(
      t(
        "Synchronization method '%method' not supported!",
        array('%method' => $method)
      ),
      'error'
    );
  }
}

/**
 * Run node sync process from a batch callback.
 *
 * Synchronize Tripal nodes with Chado data from a Drupal batch callback.
 */
function tripal_mc_run_node_sync($sync_job_args, &$context) {
  $context['message'] = 'Chado nodes synchronization...';
  // Capture Tripal output.
  ob_start();
  // Run Tripal node synchronization process.
  call_user_func_array('chado_node_sync_records', $sync_job_args);
  $output = ob_get_contents();
  ob_end_clean();

  // Display Tripal output.
  drupal_set_message(str_replace("\n", "<br/>\n", check_plain($output)));
}

/**
 * Run orphan node sync process from a batch callback.
 *
 * Remove Tripal orphan nodes through a Drupal batch callback.
 */
function tripal_mc_run_orphan_sync($orphan_job_args, &$context) {
  $context['message'] = 'Chado orphan synchronization...';
  // Capture Tripal output.
  ob_start();
  // Run Tripal node synchronization process.
  call_user_func_array('tripal_mc_cleanup_orphaned_nodes', $orphan_job_args);
  $output = ob_get_contents();
  ob_end_clean();

  // Display Tripal output.
  drupal_set_message(check_plain($output));
}

/**
 * Display sync end message.
 */
function tripal_mc_sync_finished($success, $results, $operations) {
  return t('Drupal-Chado synchronization done.');
}

/**
 * Remove Tripal orphan nodes.
 */
function tripal_mc_cleanup_orphaned_nodes($table,
  $nentries = 25000,
  $linking_table = NULL,
  $node_type = NULL,
  $job_id = NULL) {

  // The max number of records either as nodes or linked records.
  $count = 0;
  // Will hold the number of nodes of this type.
  $ncount = 0;
  // Will hold the number of linked records.
  $clcount = 0;

  if (!$node_type) {
    $node_type = 'chado_' . $table;
  }
  if (!$linking_table) {
    $linking_table = 'chado_' . $table;
  }

  $tripal_mc_linking_table = TRIPAL_MC_SYNC_TABLE_PREFIX . $linking_table;

  // Find the number nodes of type chado_$table and find the number of entries
  // in chado_$table; keep the larger of the two numbers.
  $dsql = "SELECT COUNT(1) FROM {node} WHERE type = :node_type";
  $ndat = db_query($dsql, array(':node_type' => $node_type));
  $temp = $ndat->fetchObject();
  $ncount = $temp->count;
  $clsql = "SELECT (SELECT COUNT(1) FROM {"
    . $linking_table
    . "}) + (SELECT COUNT(1) FROM {"
    . $tripal_mc_linking_table
    . "}) AS \"count\";";
  $cdat = db_query($clsql);
  $clcount = $cdat->fetchObject();
  if ($ncount < $clcount) {
    $count = $clcount;
  }
  else {
    $count = $ncount;
  }

  $transaction = db_transaction();
  print "\nNOTE: This operation is performed using a database transaction.\n";
  print "If it fails or is terminated prematurely then the entire set of \n";
  print "changes is rolled back and will not be found in the database\n\n";
  try {
    $m = ceil($count / $nentries);
    for ($i = 0; $i < $m; ++$i) {
      $offset = ($nentries * $i);
      tripal_mc_cleanup_orphaned_nodes_part(
        $table,
        $job_id,
        $nentries,
        $offset,
        $linking_table,
        $node_type
      );
    }
  }
  catch (Exception $e) {
    $transaction->rollback();
    // Make sure we start errors on new line.
    print "\n";
    watchdog_exception('trp-fsync', $e);
    print "FAILED: Rolling back database changes...\n";
  }
  return '';
}

/**
 * Remove orphaned nodes.
 *
 * This function will delete Drupal nodes for any sync'ed table (e.g.
 * feature, organism, analysis, stock, library) if the chado record has been
 * deleted or the entry in the chado_[table] table has been removed.
 *
 * @param string $table
 *   The name of the table that corresonds to the node type we want to clean up.
 * @param int $job_id
 *   This should be the job id from the Tripal jobs system.  This function
 *   will update the job status using the provided job ID.
 * @param int $nentries
 *   Number of entries to synchronize.
 * @param int $offset
 *   Offset of the starting entry to synchronize.
 * @param string $linking_table
 *   Name of the Drupal table used to link Chado data and Drupal nodes.
 * @param string $node_type
 *   Type of node to synchronize.
 *
 * @ingroup tripal_chado_node_api
 */
function tripal_mc_cleanup_orphaned_nodes_part(
  $table,
  $job_id,
  $nentries,
  $offset,
  $linking_table,
  $node_type) {

  $count = 0;
  $tripal_mc_linking_table = TRIPAL_MC_SYNC_TABLE_PREFIX . $linking_table;
  $connection_data = tripal_mc_get_user_connection();

  // Retrieve all of the entries in the linker table for a given node type
  // and place into an array.
  print "Verifying $linking_table records...\n";
  $cnodes = array();
  $query_args = array(':node_type' => $node_type);
  if (!$connection_data) {
    $clsql = "
      SELECT lt.*, n.vid
        FROM {" . $linking_table . "} lt
          LEFT JOIN {node} n ON n.nid = lt.nid
        WHERE n.type = :node_type
          OR n.nid IS NULL
      ORDER BY lt.nid LIMIT $nentries OFFSET $offset";
  }
  else {
    $clsql =
      "SELECT tmclt.*, n.vid FROM {" . $tripal_mc_linking_table
      . "} tmclt LEFT JOIN {node} n ON n.nid = tmclt.nid WHERE (n.type = :node_type OR n.nid IS NULL) AND tmclt."
      . TRIPAL_MC_CONNECTION_COLUMN
      . " = :mccid ORDER BY tmclt.nid LIMIT $nentries OFFSET $offset";
    $query_args[':mccid'] = $connection_data->mccid;
  }
  $res = db_query($clsql, $query_args);
  foreach ($res as $node) {
    $cnodes[$count] = $node;
    ++$count;
  }

  // Iterate through all of the $linking_table entries and remove those
  // that don't have a node or don't have a $table record.
  $deleted = 0;
  if ($count > 0) {
    $i = 0;
    $interval = intval($count * 0.01);
    if ($interval < 1) {
      $interval = 1;
    }
    foreach ($cnodes as $nid) {
      // Update the job status every 1% analyses.
      if ($job_id and $i % $interval == 0) {
        $percent = sprintf("%.2f", ($i / $count) * 100);
        tripal_set_job_progress($job_id, intval($percent));
        print "Percent complete: $percent%. Memory: "
          . number_format(memory_get_usage())
          . " bytes.\r";
      }

      // See if the node exits, if not remove the entry from linking table.
      if (!$nid->vid) {
        if (!$connection_data) {
          $deleted += db_delete($linking_table)
            ->condition('nid', $nid->nid)
            ->execute();
        }
        else {
          $deleted += db_delete($tripal_mc_linking_table)
            ->condition('nid', $nid->nid)
            ->execute();
        }
      }

      // Does record in chado exists, if not remove entry from $linking_table.
      $table_id = $table . "_id";
      $lsql = "SELECT * FROM {" . $table . "} WHERE "
        . $table . "_id = :" . $table . "_id";
      $results = chado_query(
        $lsql,
        array(":" . $table . "_id" => $nid->$table_id)
      );
      $record = $results->fetchObject();
      if (!$record) {
        if (!$connection_data) {
          $deleted += db_delete($linking_table)
            ->condition($table . '_id', $nid->$table_id)
            ->execute();
        }
        else {
          $deleted += db_delete($tripal_mc_linking_table)
            ->condition($table . '_id', $nid->$table_id)
            ->execute();
        }
      }
      ++$i;
    }
    $percent = sprintf("%.2f", ($i / $count) * 100);
    tripal_set_job_progress($job_id, intval($percent));
    print "Percent complete: $percent%. Memory: "
      . number_format(memory_get_usage())
      . " bytes.\r";
  }
  print "\nDeleted $deleted record(s) from $linking_table missing either a node or chado entry.\n";

  // Build the SQL statements needed to check if nodes point to valid record.
  print "Verifying nodes...\n";
  $dsql = "
    SELECT *
    FROM {node}
    WHERE type = :node_type
    ORDER BY nid
    LIMIT $nentries OFFSET $offset
  ";

  $dsql_args = array(':node_type' => $node_type);
  $nodes = array();
  $res = db_query($dsql, $dsql_args);
  $count = 0;
  foreach ($res as $node) {
    $nodes[$count] = $node;
    ++$count;
  }

  // Iterate through all of the nodes and delete those that don't have a
  // corresponding entry in the linking table.
  $deleted = 0;
  if ($count > 0) {
    $i = 0;
    $interval = intval($count * 0.01);
    if ($interval < 1) {
      $interval = 1;
    }
    foreach ($nodes as $node) {
      // Update the job status every 1%.
      if ($job_id and $i % $interval == 0) {
        $percent = sprintf("%.2f", ($i / $count) * 100);
        tripal_set_job_progress($job_id, intval($percent));
        print "Percent complete: $percent%. Memory: "
          . number_format(memory_get_usage())
          . " bytes.\r";
      }

      // Check to see if the node has a corresponding entry in the
      // $linking_table table. If not then delete the node.
      $csql = "SELECT * FROM {" . $linking_table . "} WHERE nid = :nid ";
      // Force drupal schema prefix to get ride of the view.
      $csql = "SELECT nid, ${table}_id
                 FROM {"
                 . tripal_get_schema_name('public') . '.' . $linking_table
                 . "}
                 WHERE nid = :nid
               UNION
               SELECT nid, ${table}_id
                 FROM {" . $tripal_mc_linking_table . "}
                 WHERE nid = :nid
      ;";
      $results = db_query($csql, array(':nid' => $node->nid));
      $link = $results->fetchObject();
      if (!$link) {
        // Checking node_access creates a memory leak. Commenting out for now
        // assuming that this code can only be run by a site administrator
        // anyway.
        ++$deleted;
        node_delete($node->nid);
      }
      ++$i;
    }
    $percent = sprintf("%.2f", ($i / $count) * 100);
    tripal_set_job_progress($job_id, intval($percent));
    print "Percent complete: $percent%. Memory: "
      . number_format(memory_get_usage())
      . " bytes.\r";
    print "\nDeleted $deleted node(s) that did not have corresponding $linking_table entries.\n";
  }

  return '';
}
